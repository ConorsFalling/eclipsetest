
/*
 * 
 * IMPORTANT!!!!!!!!!!!!!!!!!!
 * 
 * 
 * This java file is just an attempt to satisfy part 6, parts 3 and 4 were completed
 * in the java file "Week2", which was included this package.
 * 
 * 
 * 
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;
import java.awt.geom.Line2D;

import javax.swing.JFrame;
import javax.swing.JPanel;	

public class Cell extends JPanel{
	private int x,y;
	private int width, height;
	
	
	public Cell(int x, int y, int width, int height){
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
	}
	
	public void setX(int x){
		this.x = x;
	}
	public int getX(){
		return x;
	}
	public void setY(int y){
		this.y = y;
	}
	public int getY(){
		return y;
	}
	public void setWidth(int width){
		this.width = width;
	}
	public int getWidth(){
		return width;
	}
	public void setHeight(int height){
		this.height = height;
	}
	public int getHeight(){
		return height;
	}
	
	public void drawCell(Cell c, Graphics2D g){
		g.drawRect(c.getX(), c.getY(), c.getWidth(), c.getHeight());
	}
	
	static Cell[][] myCells = new Cell[20][20];
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		/*
		Graphics2D g2 = (Graphics2D)g;
		int[][] arr = new int[20][20];
		Random rand = new Random();
		for(int j = 0; j < 20; j++){
			for(int k = 0; k < 20; k++){
				arr[j][k] = rand.nextInt(100);
				
			}
		}
		System.out.println(arr[0][0]);
		

		g2.setColor(Color.red);
		int txtX = 22;
		int txtY = 33;
		for(int x = 0; x < 20; x++){
			for(int y = 0; y < 20; y++){
					g2.drawString(Integer.toString(arr[x][y]), txtX, txtY);
					txtX += 33;
			}
			txtX = 22;
			txtY += 33;
		}
	*/
		for(int l = 0; l <20; l++){
			for(int m = 0; m <20; m++){
				drawCell(myCells[l][m], g2);
			}
		}
		
	}
	public static void main(String [] args) {
		/*JFrame frame = new JFrame("Hello");
		frame.setSize(1280, 720);
		Cell p = new Cell(2, 2, 2, 2);
		//frame.add(p);
 
		frame.setVisible(true);
		*/
		for(int l = 0; l <20; l++){
			for(int m = 0; m <20; m++){
				int x = l * 35;
				int y = m * 35;
				myCells[l][m] = new Cell(x,y,35,35);
			}
		}
		JFrame frame = new JFrame("Hello");
		frame.setSize(1280, 720);
		frame.setResizable(false);
		frame.setVisible(true);
		
		Task3 p = new Task3();
		frame.add(p);
		frame.pack();
		frame.setVisible(true);
	}
	
	
}