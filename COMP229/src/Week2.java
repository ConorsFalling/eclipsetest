import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;
import java.awt.geom.Line2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
 

public class Week2 extends JPanel {
 
	@Override
	public void paint(Graphics g) {
		int[][] arr = new int[20][20];
		Random rand = new Random();
		for(int j = 0; j < 20; j++){
			for(int k = 0; k < 20; k++){
				arr[j][k] = rand.nextInt(100);
				
			}
		}
		System.out.println(arr[0][0]);
		Graphics2D g2 = (Graphics2D)g;
		g2.setColor(Color.blue);
		int WDist = 10;
		for(int i = 0; i <= 20; i++){
			g2.fillRect(WDist, 10, 1, 660);
			//g2.draw(new Line2D.Double(WDist, 10, 1, 700));
			g2.fillRect(10, WDist, 660, 1);
			WDist +=33;
			
		}
		
		//g2.setColor(Color.lightGray);
		//g2.fillRect(10, 10, this.getWidth() - 20, this.getHeight() - 20);
		g2.setColor(Color.red);
		int txtX = 22;
		int txtY = 33;
		for(int x = 0; x < 20; x++){
			for(int y = 0; y < 20; y++){
					g2.drawString(Integer.toString(arr[x][y]), txtX, txtY);
					txtX += 33;
			}
			txtX = 22;
			txtY += 33;
		}
 
		
	}

	public static void main(String [] args) {
		JFrame frame = new JFrame("Hello");
		frame.setSize(1280, 720);
 
		Week2 p = new Week2();
		frame.add(p);
 
		frame.setVisible(true);
	}
	
}