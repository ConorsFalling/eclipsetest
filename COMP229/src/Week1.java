
public class Week1 {

	public static void main(String [] args)
	{
		System.out.println("~~~~~");
		System.out.println("Sheep and Wolves");
		System.out.println("~~~~~");
	}
	
	/*# Task 0

	Clone this repository (well done!)

	# Task 1

	Take a look a the two repositories:
	  
	  * (A) https://bitbucket.org/farleyknight/ruby-git
	  * (B) https://bitbucket.org/kennethendfinger/git-repo

	And answer the following questions about them:

	  * Who made the last commit to repository A?
	  farleyknight
	  * Who made the first commit to repository A?
	  scott Chacon
	  * Who made the last and first commits to repository B?
	  Shawn, The Android Open Source Project
	  * Who has been the most active recent contributor on repository A?  How about repository B?
	  scott, shawn
	  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
	  Both have been inactive since 2012, 
	  git-repo reached relaced a stable version and bug fixed for a while, I assume the project was completed. 
	  ruby-git also past their 1.0 version, so probably finished enough for the developers.
	  * 🤔 Which file in each project has had the most activity?
	  ruby: lib.rb
	  git: project.py
	  

	# Task 2

	Setup a new eclipse project with a main method that will print the following message to the console when run:

	~~~~~
	Sheep and Wolves
	~~~~~ 

	🤔 Now setup a new bitbucket repository and have this project pushed to that repository.  You will first need to `commit`, then `push`.  Ensure you have setup an appropriate `.gitignore` file.  The one we have in this repository is a very good start.
	
*/
	
}
